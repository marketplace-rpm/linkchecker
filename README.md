# Information / Информация

SPEC-файл для создания RPM-пакета **linkchecker**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/linkchecker`.
2. Установить пакет: `dnf install linkchecker`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)